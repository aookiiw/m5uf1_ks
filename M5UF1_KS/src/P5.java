
import java.util.Scanner;
public class P5 {

    private Scanner input = new Scanner(System.in);

    public void showMenu(){
        System.out.println("Escoge una opcion:");
        System.out.println("1.Hello World");
        System.out.println("2.SumAdd");
        System.out.println("3.Find Max");
        System.out.println("4.Find Min");
        System.out.println("5.Add Array");
        System.out.println("6.Palindrom");
        System.out.println("0. Salir\n");
    }

    public int readOpcion(){
        return input.nextInt();
    }

    public void operar(int opcion){

        switch (opcion){
            case 0:
                exit();
                break;
            case 1: helloWorld();
                break;
            case 2:
                sumAdd_KS();
                break;
            case 3:
                findMax_KS();
                break;
            case 4:
                findMin_KS();
                break;
            case 5:
                addArray_KS();
                break;
            case 6:
                palindrom_ks();
                break;
        }
    }

    /* OPERATIONS */
    public void exit(){
        System.out.println("END");
    }
    public void helloWorld(){
        System.out.println("Hello World");
    }
    public void sumAdd_KS(){
        System.out.println("First number: ");
        int num1 = input.nextInt();
        System.out.println("Second number: ");
        int num2 = input.nextInt();
        int sum = num1+num2;
        System.out.println("Result of that operation is: " + sum);
        System.out.println();
    }
    public void findMax_KS(){
        System.out.println("First number: ");
        int num1 = input.nextInt();
        System.out.println("Second number: ");
        int num2 = input.nextInt();

        if (num1 >= num2){
            System.out.println(num1 + " is major than " + num2);
        } else {
            System.out.println(num2 + " is major than " + num1);
        }

    }
    public void findMin_KS(){
        System.out.println("First number: ");
        int num1 = input.nextInt();
        System.out.println("Second number: ");
        int num2 = input.nextInt();

        if (num1 >= num2){
            System.out.println(num2 + " is lower than " + num1);
        } else {
            System.out.println(num1 + " is lower than " + num2);
        }

    }
    public void addArray_KS(){
        System.out.println("Input numbers for the first array");
        int[] array1 = new int[5];
        for (int i = 0; i < array1.length; i++) {
            System.out.println("Element #" + i);
            array1[i] = input.nextInt();
        }
        System.out.println("Input numbers for the second array");
        int[] array2 = new int[5];
        for (int i = 0; i <= array2.length; i++) {
            System.out.println("Element #" + i);
            array2[i] = input.nextInt();
        }
        int[] array3 = new int[5];
        for (int i = 1; i <= array3.length; i++) {
            System.out.println("Result for an element #" + i + " in array 3:");
            array3[i] = array1[i] + array2[i];
            System.out.println(array3[i]);
        }
    }
    public void palindrom_ks(){
        System.out.println("Introduce text: ");
        input.nextLine();
        String text = input.nextLine();

        StringBuilder textnew = new StringBuilder();

        for (int i = text.length()-1; i >= 0; i--) {
            textnew.append(text.charAt(i));
        }

        if (textnew.toString().equals(text)){
            System.out.println("It's palindrom");
        } else {
            System.out.println("It's not a palindrom. New text is: " + textnew.toString());
        }

    }
    public static void main(String[] args) {

        P5 p5 = new P5();

        int response = 0;
        do{
            p5.showMenu();
            response = p5.readOpcion();
            p5.operar(response);
        }while(response != 0);

    }
}
